import { createApp } from 'vue'
import App from './App.vue'
import {message} from "@/data";
import {data} from "@/data"
import printData from "@/console";

createApp(App).mount('#app')

printData(message)
printData(data)
